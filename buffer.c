/*
 * =====================================================================================
 *
 *       Filename:  buffer.c
 *
 *    Description: buffer cache implemaintion 
 *
 *        Version:  1.0
 *        Created:  Thursday 06 August 2020 04:17:35  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */
#include "buffer.h"

buf* get_buffer_from_head_of_free_list()
{
	if(is_free_list_empty())
	{
		return NULL;
	}
	buf* ret_buf = free_list_dhead->p_next_free_list;
	while(is_in_state(ret_buf,BUFFER_DELAYED_WRITE))
	{
		ret_buf = ret_buf->p_next_free_list;
	}
	remove_buf_from_hash_queue(ret_buf);
	free_list_dhead->p_next_free_list = free_list_dhead->p_next_free_list->p_next_free_list;
	free_list_dhead->p_next_free_list->p_prev_free_list =free_list_dhead;
	return ret_buf;
}
void uninitlize_ds()
{
	for(int i = NO_OF_HASH_QUEUES - 1; i >= 0; i--)
	{
		while(true)
		{
			if(hash_queue[i]->p_prev_hash_queue_buff != hash_queue[i])
			{
				buf* temp = hash_queue[i]->p_prev_hash_queue_buff;
				remove_buf_from_hash_queue(temp);
				if(!search_buffer_in_free_list(temp))
				{
					add_buf_to_free_list(temp);
				}
			}
			else
			{
				xfree(hash_queue[i]);
				break;
			}
		}
	}
	while(true)
	{
		if(free_list_dhead->p_prev_free_list != free_list_dhead)
		{
			buf* temp = free_list_dhead->p_prev_free_list;
			remove_buffer_from_free_list(temp);
			xfree(temp);
		}
		else
		{
			xfree(free_list_dhead);
			xfree(hash_queue);
			break;
		}
	}
	delete_waiting_queue();
	return;
}
void add_buf_to_hash_queue(buf* buffer)
{
	int key = buffer->block_num % NO_OF_HASH_QUEUES;
	add_state(buffer,BUFFER_BUSY);
	hash_queue_push_back(hash_queue[key],buffer);
}
void add_buf_to_free_list(buf* buffer)
{
	remove_state(buffer, BUFFER_BUSY);
	free_list_push_back(free_list_dhead,buffer);
}

buf* search_block_in_hash_queue(int blk_num)
{
	int hash_key = blk_num % NO_OF_HASH_QUEUES;
	for(buf* p = hash_queue[hash_key]->p_next_hash_queue_buff; p != hash_queue[hash_key]; p = p->p_next_hash_queue_buff)
	{
		if(p->block_num == blk_num)
		{
			return p;
		}
	}
	return NULL;
}
bool search_buffer_in_free_list(buf* buffer)
{
	for(buf* p = free_list_dhead->p_next_free_list; p != free_list_dhead; p = p->p_next_free_list)
	{
		if(p == buffer)
		{
			return true;
		}
	}
	return false;
}
bool is_hash_queue_empty(int key)
{
	key %= NO_OF_HASH_QUEUES;
	if((hash_queue[key]->p_next_hash_queue_buff == hash_queue[key]) && 
		(hash_queue[key]->p_prev_hash_queue_buff == hash_queue[key]))
	{
		return true;
	}
	return false;
}
bool is_free_list_empty()
{
	if((free_list_dhead->p_prev_free_list == free_list_dhead)&& (free_list_dhead->p_next_free_list == free_list_dhead))
	{
		return true;
	}
	return false;
}
void remove_buf_from_hash_queue(buf* temp_buf)
{
	buf* next_buf = temp_buf->p_next_hash_queue_buff;
	buf* prev_buf = temp_buf->p_prev_hash_queue_buff;
	next_buf->p_prev_hash_queue_buff= prev_buf;
	prev_buf->p_next_hash_queue_buff= next_buf;
	temp_buf->p_next_hash_queue_buff = NULL;
	temp_buf->p_prev_hash_queue_buff = NULL;
}	
void hash_queue_push_front(buf* dummy_head, buf* temp_buf)
{
	temp_buf->p_prev_hash_queue_buff = dummy_head;
	temp_buf->p_next_hash_queue_buff = dummy_head->p_next_hash_queue_buff;
	dummy_head->p_next_hash_queue_buff->p_prev_hash_queue_buff = temp_buf;
	dummy_head->p_next_hash_queue_buff = temp_buf;
	return;
}
void hash_queue_push_back(buf* dummy_head, buf* temp_buf)
{
	temp_buf->p_next_hash_queue_buff = dummy_head;
	temp_buf->p_prev_hash_queue_buff = dummy_head->p_prev_hash_queue_buff;
	dummy_head->p_prev_hash_queue_buff->p_next_hash_queue_buff = temp_buf;
	dummy_head->p_prev_hash_queue_buff= temp_buf;
	return ;
}
void free_list_push_front(buf* dummy_head, buf* temp_buf)
{
	temp_buf->p_prev_free_list = dummy_head;
	temp_buf->p_next_free_list = dummy_head->p_next_free_list;
	dummy_head->p_next_free_list->p_prev_free_list = temp_buf;
      dummy_head->p_next_free_list = temp_buf;	
	return;
}
void free_list_push_back(buf* dummy_head,buf* temp_buf)
{
	temp_buf->p_next_free_list = dummy_head;
	temp_buf->p_prev_free_list = dummy_head->p_prev_free_list;
	dummy_head->p_prev_free_list->p_next_free_list = temp_buf;
	dummy_head->p_prev_free_list = temp_buf;
	return;
}

void remove_buffer_from_free_list(buf* rem_buf)
{
	buf* next = rem_buf->p_next_free_list;
	buf* prev = rem_buf->p_prev_free_list;
	prev->p_next_free_list = next;
	next->p_prev_free_list = prev;
	rem_buf->p_next_free_list = NULL;
	rem_buf->p_prev_free_list = NULL;
}

void xfree(void* rem_buf)
{
	if(rem_buf)
	{
		free(rem_buf);
		rem_buf = NULL;
	}
}

void* xmalloc(size_t size)
{
	void* temp = malloc(size);
	assert(temp);
	return temp;
}


