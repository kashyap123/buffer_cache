/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  Entry point function
 *
 *        Version:  1.0
 *        Created:  Thursday 06 August 2020 04:19:42  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */
#include "buffer.h"
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <getopt.h>
#include <signal.h>
#include <errno.h>
#define COMMAND_SIZE 100
void init_buffer(void);
void init_ds(void);
bool should_run = true;
static volatile sig_atomic_t is_interrupted = 0;

static int init_sig(void); 

void sig_int_handler(int num) 
{
	fprintf(stdout,"Interrupt recevied\n");
    	is_interrupted = 1;
	should_run = false;
}

int main(int argc,char**argv)
{
	char str[COMMAND_SIZE] = {0};
	char *shell_argv[COMMAND_SIZE] = {0};
	int i, wc;
     	init_buffer();	
	/* Configure signals */
	if(init_sig() != 0)
	{
		fprintf(stderr, "Unable to init signal \r\n");
	    return 1;
	}
	while(should_run)
	{
		fprintf(stdout,"\033[0;32m");//set text color to green
		fprintf(stdout,"sh:~$\033[0m");//reset text color 
		if(fgets(str,COMMAND_SIZE,stdin) == NULL)
		{
			//strcpy(str,"exit");
			continue;
		}	
		char *q,*p = malloc(strlen(str) * sizeof(char));
		strcpy(p,str);
		int nfield = 0;
		//for(;(q = strtok(p," \n\r")) != NULL;p = NULL )
		while((q = strtok(p," \n\r"))!= NULL)
		{
			shell_argv[nfield++] = q;
			p = NULL;
		}
		free(p);
		if(shell_argv[0] == '\0')
		{
			continue;
		}
		if(!strcmp(shell_argv[0],"clear"))
		{
			fprintf(stdout,"\e[1;1H\e[2J");//clear screen
		}
		else if(!strcmp(shell_argv[0],"exit"))
		{
			should_run = false;
		}
		else
		{
			fprintf(stdout,"%s: command not found\n",shell_argv[0]);
		}
	}
	fprintf(stdout,"wait...cleaning ds\n");
	usleep(5000000);
	uninitlize_ds();
	return 0;
}
static int init_sig(void) 
{
	
    	int ret;
	struct sigaction sigaction_new, sigaction_old;
	sigset_t sig_set;

	/* Install signal handler for SIGINT */
	/* Block all signals */
	sigfillset(&sig_set);
	ret = pthread_sigmask(SIG_BLOCK, &sig_set, NULL);
	if(ret != 0) 
	{
	  	errno = ret;
		perror("Unable to set thread signal mask\r\n");
		return -1;
	}

	/* Install handler for SIGINT */
	sigaction_new.sa_handler = &sig_int_handler;
	sigaction_new.sa_flags = 0;
	sigfillset(&sigaction_new.sa_mask);

	ret = sigaction(SIGINT, &sigaction_new, &sigaction_old);
	if(ret != 0) 
	{
	  	fprintf(stderr, "Unable to install new signal handler: %s\r\n"
			   , strerror(errno));
		return -1;
	}

	/* Enable SIGINT only */
	sigemptyset(&sig_set);
	sigaddset(&sig_set, SIGINT);

	ret = pthread_sigmask(SIG_UNBLOCK, &sig_set, NULL);
	if(ret != 0) 
	{
	  	errno = ret;
		perror("Unable to set thread signal mask\r\n");
		return -1;
	}

	return 0;
}

void init_buffer(void)
{
	init_ds();
	create_waiting_queue();
}

void init_ds(void)
{
	hash_queue = (buf**)xmalloc(sizeof(buf*)*NO_OF_HASH_QUEUES);
	for(int i = 0; i < NO_OF_HASH_QUEUES; i++)
	{
		//demo node
		hash_queue[i] = (buf*)xmalloc(sizeof(buf));	
		//setting up node
		hash_queue[i]->p_next_hash_queue_buff = hash_queue[i];
		hash_queue[i]->p_prev_hash_queue_buff = hash_queue[i];
		hash_queue[i]->p_next_free_list = free_list_dhead;
		hash_queue[i]->p_prev_free_list = free_list_dhead;

	}
	printf("Creating Free list....\n");
	free_list_dhead = (buf*)xmalloc(sizeof(buf));
	free_list_dhead->p_next_free_list = free_list_dhead;
	free_list_dhead->p_prev_free_list = free_list_dhead;
	//creating free node list
	for(int i = 0; i < NO_OF_FREE_NODE; i++)
	{
		buf* temp = (buf*)xmalloc(sizeof(buf));
		bzero(temp,sizeof(buf));
		free_list_push_back(free_list_dhead,temp);
	}


}
