/*
 * =====================================================================================
 *
 *       Filename:  state_manager.c
 *
 *    Description:  state of buffers
 *
 *        Version:  1.0
 *        Created:  Thursday 06 August 2020 04:20:51  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */


#include "buffer.h"

/////////////////////////////////
//write state manager
//Add this state to buffer status
void add_state(buf *buffer, status state)
{
	buffer->state = (buffer->state) | state;
}
//Remove this state from buffer status
void remove_state(buf* buffer, status state)
{
	buffer->state = buffer->state ^ state; 
}
//ser only this state as the buffer status
void set_state(buf* buffer, status state)
{
	buffer->state = state;
}
//check if buffer is in the given state
int is_in_state(buf* buffer, status state)
{
	return (buffer->state & state);
}
/////////////////////////////////
