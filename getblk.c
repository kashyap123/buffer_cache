/*
 * =====================================================================================
 *
 *       Filename:  getblk.c
 *
 *    Description:  get block 
 *
 *        Version:  1.0
 *        Created:  Sunday 09 August 2020 09:27:10  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */

#include "buffer.h"

buf* getblk(int part_num, int block_num)
{
	while(true)
	{
		buf* buffer = search_block_in_hash_queue(block_num);
		/* *
		 * Scenario 5 
		 * search buffer in hash queue
		 * */
		if(buffer != NULL) //if buffer in hash queue
		{
			if(is_in_state(buffer,BUFFER_BUSY))
			{
				printf("********** Scenario 5 *********");
				printf("The Kernel finds the block on the hash queue, but its buffer is currently busy\n");
				printf("Process going to sleep\n");
				printf("process will wake up when this buffer becaome free\n");
				//set condition variable for process or thread
				//sleep() (event: buffer become free);
				//continue; //back to while loop
				return NULL;
			}
			/* *
			 * Scenario 1
			 * buffer is free
			 * */
			printf("********* Scenario 1 *********\n");
			printf("kernel find the buffer on its hash queue and that buffer is free\n");
			set_state(buffer,BUFFER_BUSY | BUFFER_VALID);
			printf("Buffer marked busy\n");
			remove_buffer_from_free_list(buffer);
			printf("remove buffer from free list\n");
			return buffer;
		}
		else // Block is not on hash queue
		{
			if(is_free_list_empty()) //if therer are no buffers in free list 
				//Scenario 4
			{
				printf("The kerenl cannot find the block on the hash queue , and the free list is empty\n");
				printf("Process goes to sleep\n");
				printf("Process will wake up when any buffer becomes free\n");
				//sleep()(event: any buffer becomes free)
				//continue;
				return NULL;
			}
			buf* ref = free_list_dhead->p_next_free_list;
			if(is_in_state(ref,BUFFER_DELAYED_WRITE)) //Buffer marked delayed write Scenario 3
			{
				printf("********* Scenario 3 *********\n");
				printf("The kernel cannot find the block on the hash queue. Found free block is marked delayed write.\n");
				buf* next = ref->p_next_free_list;
				buf* prev = ref->p_prev_free_list;
				prev->p_next_free_list = next;
				next->p_prev_free_list = prev;
				set_state(ref,BUFFER_BUSY | BUFFER_VALID | BUFFER_DELAYED_WRITE | BUFFER_DATA_OLD);
				printf("marking buffer old. writeing asynchronously\n");
				continue;
			}
			// Scenario 2
			// found a free buffer
			printf("********* Scenario 2 *********\n");
			printf("The kernel cannot find the block on the hash queue, so it allocates a buffer from the free list\n");
			buf* free_buf = get_buffer_from_head_of_free_list();
			 remove_state(free_buf,BUFFER_VALID);
			 printf("Remove from old hash queue.\n putting in new hash queue\n");
			 free_buf->block_num = block_num;
			 add_buf_to_hash_queue(free_buf);
			 printf("Kernel disk access occuring. Status of Block Updated.\n");
			 add_state(free_buf,BUFFER_READING_WRITING);
			 //wait till fill the buffer
			 printf("Kernel Disk access finished. status of block updated\n");
			 remove_state(free_buf,BUFFER_READING_WRITING);
			 printf("Buffer now contains new and valid data. Updating status\n");
			 add_state(free_buf,BUFFER_VALID);
			 return free_buf;
		}
	}	
	printf("Buffer Not found\n");
	return NULL;
}
