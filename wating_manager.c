/*
 * =====================================================================================
 *
 *       Filename:  wating_manager.c
 *
 *    Description:  waiting buffer array
 *
 *        Version:  1.0
 *        Created:  Wednesday 19 August 2020 11:20:55  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */

#include "buffer.h"

static int total_wait = -1;

/**
 * @brief
 *
 */
void create_waiting_queue()
{
	waiting_queue = (int*)xmalloc(SIZE_OF_WAITING_QUEUE * sizeof(int));
	cv = (pthread_cond_t*)xmalloc(SIZE_OF_WAITING_QUEUE * sizeof(pthread_cond_t));
	memset(waiting_queue,0,sizeof(waiting_queue));
	memset(cv,0,sizeof(cv));
}

/**
 * @brief 
 * deleteing wait queue
 * @return nothing
 */
void delete_waiting_queue()
{
	xfree(waiting_queue);
	xfree(cv);
}

/**
 * @brief 
 *
 * @param blk_num
 *
 * @return 
 */
bool add_to_wait_queue(int blk_num,int *wait_id)
{
	
	if(total_wait >= SIZE_OF_WAITING_QUEUE)
	{
		print_log("There is no space for waiting","INFO");
		return false;
	}
	int n = search_wait_id_using_blk_num(blk_num);
	if( n > 0)
	{
		print_log(INFO,"already %d position buffer waiting for %d block number",n,blk_num);
		*wait_id = n;
		return true;		
	}
	waiting_queue[total_wait] = blk_num;
	*wait_id = total_wait;
	total_wait = total_wait + 1;
	return true;
}

int search_wait_id_using_blk_num(int blk_num)
{
	if(waiting_queue[total_wait] == blk_num)
	{
		return total_wait;
	}
	int bkc = waiting_queue[total_wait];
	waiting_queue[total_wait] = blk_num;

	for(int i = 0;;i++)
	{
		if(waiting_queue[i] == blk_num)
		{
			waiting_queue[total_wait] = 	bkc;

			if(i < total_wait)
			{
				return i;
			}
			return -1;
		}
	}
		
}

bool wake_up_thread(int blk_num)
{
	int wait_id = search_wait_id_using_blk_num(blk_num);
	if(wait_id < 0)
	{
		char str[50];
		
		print_log(INFO,"There is no such block_num");
		return false;
	}
	pthread_cond_broadcast(&cv[wait_id]);
	return true;
}

void wake_up_all_threads()
{
	if(total_wait < 0)
	{
		print_log(WARN,"There is no thread for waiting for any buffer");
		return ;
	}
	for(int i = 0; i < total_wait; i++)
	{
		pthread_cond_broadcast(&cv[i]);
	}
	return;
}
