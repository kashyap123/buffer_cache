/*
 * =====================================================================================
 *
 *       Filename:  in_inode.h
 *
 *    Description:  header of in core inode structure
 *
 *        Version:  1.0
 *        Created:  Monday 24 August 2020 04:56:15  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdbool.h>
#include "log_manager.h"

typedef struct in_inode
{
	char owner[10];
	char group[10];
	char file_type;
	//save permission
	char a_time[20];
	char m_time[20];
	char c_time[20];
	int no_of_links;
	int size_in_bytes;
	int disk_block[10];
	bool is_locked;
	bool is_reserved;
	bool is_inode_info_changed;
	bool is_file_field_changed;
	//more member to be added
	struct in_inode * hash_next;
	struct in_inode * hash_prev;
	struct in_inode * free_next;
	struct in_inode * free_prev;
}in_core_inode;

in_core_inode **inode_hash_queue;
in_core_inode *inode_free_dhead;

