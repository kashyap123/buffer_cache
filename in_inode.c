/*
 * =====================================================================================
 *
 *       Filename:  in_inode.c
 *
 *    Description:  in core inode table	
 *
 *        Version:  1.0
 *        Created:  Monday 24 August 2020 02:40:42  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */


#include "in_inode.h"

void free_list_inode_push_back(in_core_inode* dummy_head,in_core_inode *temp_inode)
{
	temp_inode->free_next = dummy_head;
	temp_inode->free_prev = dummy_head->free_prev;
	dummy_head->free_prev->free_next = temp_inode;
	dummy_head->free_prev = temp_inode;
}

void free_list_inode_push_front(in_core_inode* dummy_head,in_core_inode *temp_inode)
{
	temp_inode->free_next = dummy_head->free_next;
	dummy_head->free_next->free_prev = temp_inode;
	dummy_head->free_next = temp_inode;
	temp_inode->free_prev = dummy_head;
}

void hash_queue_inode_push_back(in_core_inode* dummy_head, in_core_inode* temp_inode)
{
	temp_inode->hash_next = dummy_head;
     	temp_inode->hash_prev = dummy_head->hash_prev;
	dummy_head->hash_prev->hash_next = temp_inode;
	dummy_head->hash_prev = temp_inode;	
}

void hash_queue_inode_push_front(in_core_inode* dummy_head, in_core_inode* temp_inode)
{
	temp_inode->hash_next = dummy_head->hash_next;
	dummy_head->hash_next->hash_prev = temp_inode;
	temp_inode->hash_prev = dummy_head;
	dummy_head->hash_next = temp_inode;
}

void remove_inode_from_free_list(in_core_inode * temp_inode)
{
	in_core_inode *next = temp_inode->free_next;
	in_core_inode *prev = temp_inode->free_prev;

	next->free_prev = prev;
	prev->free_next = next;
	temp_inode->free_next = NULL;
	temp_inode->free_prev = NULL;
}
void remove_inode_from_hash_queue(in_core_inode* temp_inode)
{
	in_core_inode *next = temp_inode->hash_next;
	in_core_inode *prev = temp_inode->hash_prev;

	next->hash_prev = prev;
	prev->hash_next = next;

	temp_inode->hash_next = NULL;
	temp_inode->hash_prev = NULL;
}

bool is_free_inode_list_empty()
{
	if(inode_free_dhead->free_next == inode_free_dhead && inode_free_dhead->free_prev == inode_free_dhead)
	{
		return true;
	}
	return false;
}
	
bool is_hash_inode_queue_empty(int key)
{
	if(inode_hash_queue[key]->hash_next == inode_hash_queue[key] && inode_hash_queue[key]->hash_prev == inode_hash_queue[key])
	{
		return true;
	}	
	return false;
}

bool search_inode_in_free_list(in_core_inode* ino_buf)
{
	for(in_core_inode* p = in_core_inode->free_next; p != inode_free_dhead; p = p->free_next)
	{
		if(p == ino_buf)
		{
		 	return true;
		}
	}	
	return false;
}

