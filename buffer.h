/*
 * =====================================================================================
 *
 *       Filename:  buffer.h
 *
 *    Description: buffer headers 
 *
 *        Version:  1.0
 *        Created:  Thursday 06 August 2020 04:17:35  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <pthread.h>
#include <string.h>
#define BUF_SIZ	1024
#define NO_OF_HASH_QUEUES 4
#define NO_OF_FREE_NODE 40
#define SIZE_OF_WAITING_QUEUE NO_OF_FREE_NODE/4
#define INFO "INFO"
#define ERROR "ERROR"
#define WARN "WARN"

int *waiting_queue;
pthread_cond_t *cv;
pthread_mutex_t lock;
typedef enum status
{
	BUFFER_BUSY = 1,
	BUFFER_VALID = 2,
	BUFFER_DELAYED_WRITE = 4,
	BUFFER_READING_WRITING = 8,
	BUFFER_AWAITED = 16,
	BUFFER_DATA_OLD = 32,
}status;

typedef struct buffer_cache
{
	int dev_id; //partition number	
	int block_num; // logical block number on partition
	unsigned int state; //buffer status
	char data[BUF_SIZ]; // actual data
	struct buffer_cache *p_next_hash_queue_buff; //HASH QUEUE list next
	struct buffer_cache *p_prev_hash_queue_buff;//HASH QUEUE list prev
	struct buffer_cache *p_next_free_list;//free list next
	struct buffer_cache *p_prev_free_list;//free list prev	
}buf;
buf** hash_queue;
buf*  free_list_dhead;
void* xmalloc(size_t size);
buf* getblk(int part_num, int block_num);
buf* get_buffer_from_head_of_free_list();
void remove_buffer_from_free_list(buf* rem_buf);
void free_list_push_back(buf* dummy_head,buf* temp_buf);
void free_list_push_front(buf* dummy_head, buf* temp_buf);
void hash_queue_push_back(buf* dummy_head, buf* temp_buf);
void hash_queue_push_front(buf* dummy_head, buf* temp_buf);
void remove_buf_from_hash_queue(buf* temp_buf);
bool is_free_list_empty();
bool is_hash_queue_empty(int key);
bool search_buffer_in_free_list(buf* buffer);
buf* search_block_in_hash_queue(int blk_num);
void add_buf_to_free_list(buf* buffer);
void add_buf_to_hash_queue(buf* buffer);
void set_state(buf* buffer, status state);
int is_in_state(buf* buffer, status state);
void add_state(buf *buffer, status state);
void remove_state(buf* buffer, status state);
void xfree(void* rem_buf);
void uninitlize_ds();
void print_log(char* log_type,char*fmt,...);
void create_waiting_queue();
void delete_waiting_queue();
bool add_to_wait_queue(int blk_num,int *wait_id);
int search_wait_id_using_blk_num(int blk_num);
bool wake_up_thread(int blk_num);
void wake_up_all_threads();
